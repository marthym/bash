# Scripts Bash

## backup-dir.sh

Backup all directories in the given `$BACKUP_DIR`:

  * Compress
  * Crypt by GPG
  * Upload to HubiC

Script designed for by year directory structure.

    Usage: ./backup-dir.sh "gpg-passphrase" "backup-directory" 
    Description: Compress, crypt and upload each directory to HubiC
    Examples: ./backup-dir.sh "xxxxx" "~/photos" 
    Options:
      --help: Display this help message


## git-repos-update-all.sh

Find all git repository from `~/` and update them in multi-thread mode.

    Usage:       git-repos-update-all.sh
    Version:     2.0
    Description: Find all git repository from `~/` and update them in multi-thread mode.
    Options:
      --help:     Display this help message
