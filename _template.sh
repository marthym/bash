#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

#/ Usage:       _template.sh
#/ Version:     1.0
#/ Description: Template de script bash
#/ Examples:
#/ Options:
#/   -h|--help:     Display this help message
#/   -v|--verbose:  Display DEBUG log messages
function usage() { grep '^#/' "$0" | cut -c4- ; exit 0 ; }

#######################################################
## LOGGING FRAMEWORK
readonly NORMAL=""
readonly RED="\e[1;31m"
readonly YELLOW="\e[1;33m"
readonly DIM="\e[2m"
readonly LOG_FILE="/tmp/$(basename "$0").log"
function log() {
  ( flock -n 200
    color="$1"; level="$2"; message="$3"
    printf "${color}%-9s %s\e[m\n" "[${level}]" "$message" | tee -a "$LOG_FILE" >&2 
  ) 200>"/var/lock/.$(basename "$0").log.lock"
}
function debug() { if [ "$verbose" = true ]; then log "$DIM"    "DEBUG"   "$*"; fi }
function info()  { log "$NORMAL" "INFO"    "$*"; }
function warn()  { log "$YELLOW" "WARNING" "$*"; }
function error() { log "$RED"    "ERROR"   "$*"; }
function fatal() { log "$RED"    "FATAL"   "$*"; exit 1 ; }
#######################################################

function cleanup() {
    # Remove temporary files
    # Restart services
    # ...
    return
}

if [[ "${BASH_SOURCE[0]}" = "$0" ]]; then
    trap cleanup EXIT

    # Parse command line arguments
    POSITIONAL=()
    verbose=false
    while [[ $# -gt 0 ]]; do
        key="$1"
        case $key in
            -h|--help)
            usage
            ;;
            -v|--verbose)
            declare -r verbose=true
            shift
            ;;
            *)    # unknown option
            POSITIONAL+=("$1") # save it in an array for later
            shift # past argument
            ;;
        esac
    done
    set -- "${POSITIONAL[@]}" # restore positional parameters

    # Script goes here
    # ...
fi
