#!/usr/bin/env bats

source _template.sh

set +u 

function teardown() {
    rm -rf "/tmp/bats-exec-test.log"
}

@test "should display info logs" {
    run info "test info"
    echo $output

    [ "$status" -eq 0 ]
    [[ "$output" == *"[INFO]    test info"* ]]

    lastline=$(tail -1 "/tmp/bats-exec-test.log")
    [[ "$lastline" == "$output" ]]
}

@test "should display warning logs" {
    run warn "test warning"
    echo "$output"

    [ "$status" -eq 0 ]
    [[ "$output" == *"[WARNING] test warning"* ]]

    lastline=$(tail -1 "/tmp/bats-exec-test.log")
    [[ "$lastline" == "$output" ]]
}

@test "should display error logs" {
    run error "test error"
    echo "$output"

    [ "$status" -eq 0 ]
    [[ "$output" == *"[ERROR]   test error"* ]]

    lastline=$(tail -1 "/tmp/bats-exec-test.log")
    [[ "$lastline" == "$output" ]]
}

@test "should display fatal logs" {
    run fatal "test fatal"
    echo "$output"

    [ "$status" -eq 1 ]
    [[ "$output" == *"[FATAL]   test fatal"* ]]

    lastline=$(tail -1 "/tmp/bats-exec-test.log")
    [[ "$lastline" == "$output" ]]
}