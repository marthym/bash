#!/usr/bin/env bats

source backup-dir.sh

set +u 

function setup() {
    bckpDirectory=$(mktemp -d)
    toArchiveDirectory="(test) sp,a?c'e"

    mkdir -p "$bckpDirectory/$toArchiveDirectory"
    echo "bla bla" >> "$bckpDirectory/$toArchiveDirectory/fileToBackup.txt"
    echo "c7254d729131380f95a0e8fbcf1a518525afbcfa (test) sp,a?c'e" >> $(_sha1sumFile "$bckpDirectory")
    echo "5cd337198ead0768975610a135e2625715319842 (test) not existing dir" >> $(_sha1sumFile "$bckpDirectory")
}

function teardown() {
    if [ ! -v "$bckpDirectory" ]; then rm -rf "$bckpDirectory"; fi
}

@test "should clean current dirmane" {
    run _filename "test/[test] (test) sp,a?c'e/"
    echo "$output"
    [ "$status" -eq 0 ]
    [ "$output" = "test[test]testspace" ]
}

@test "should check directory sha1" {
    output=$(_checkDirectorySha1 "$bckpDirectory" "$toArchiveDirectory")
    lines=($output)
    status=$?

    [[ "$status" == 0 ]]
    [[ "${lines[0]}" == "c7254d729131380f95a0e8fbcf1a518525afbcfa" ]]
    [[ "${lines[1]}" == "OK" ]]
}

@test "should check updated directory sha1" {
    echo "bla bla" >> "$bckpDirectory/$toArchiveDirectory/fileToBackup.txt"

    output=$(_checkDirectorySha1 "$bckpDirectory" "$toArchiveDirectory")
    lines=($output)
    status=$?

    [[ "$status" == 0 ]]
    [[ "${lines[0]}" == "73ce4f29a9482a2ab03883c80d94660357237ab7" ]]
    [[ "${lines[1]}" == "CHANGED" ]]

}

@test "should create GPG archive" {
    output=$(_createArchive "$bckpDirectory" "$toArchiveDirectory" "passphrase")
    status=$?

    [[ "$status" == 0 ]]
    [[ "$output" == "testspace.tar.bz2.gpg" ]]
    [ -f "/tmp/${output}" ]
    [ -s "/tmp/${output}" ]
}

@test "should clean sha1 file" {
    _cleanSha1File "$bckpDirectory"
    status=$?

    sha1file=$(_sha1sumFile "$bckpDirectory")
    output=$(wc -l < "$sha1file")

    [[ "$status" == 0 ]]
    [[ "$output" == 1 ]]
}